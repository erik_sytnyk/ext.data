﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Ext.Core;
using Ext.Data.Base;
using Ext.Data.Base.Infrastructure;
using StubDb;

namespace Ext.Data.Stubs
{
    public abstract class BaseStubRepository<TEntity, TContext> : IRepositoryBase<TEntity>
        where TEntity : class 
        where TContext : StubContext, new()
    {
        private TContext _dataContext;

        protected BaseStubRepository(TContext context)
        {
            _dataContext = context;
        }

        public TContext DataContext
        {
            get { return _dataContext ?? (_dataContext = new TContext()); }
        }

        protected StubSet<TEntity> Entities
        {
            get 
            {
                var entitiesSet = DataContext.GetStubSet<TEntity>();
                var notFoundException = String.Format("Cannot find StubSet of type {0}. Make sure it is registed in context class.", typeof (TEntity).FullName);
                Check.NotNull(entitiesSet, notFoundException);
                return entitiesSet;
            }
        }

        #region IRepositoryBase<T> Members

        public virtual void Add(TEntity entity)
        {
            Entities.Add(entity);
        }

        public virtual void Update(TEntity entity)
        {
            Entities.Update(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            Entities.Remove(entity);
        }

        public void Delete(int id)
        {
            Entities.Remove(id);
        }

        public void Delete(Expression<Func<TEntity, bool>> condition)
        {
            var entitiesToDelete = Entities.Query().Where(condition);
            foreach (var entity in entitiesToDelete)
            {
                this.Delete(entity);
            }
        }

        public virtual TEntity GetById(int id)
        {
            var result = Entities.Query().SingleOrDefault(x => TypeHelper.GetEntityId(x) == id);
            return result;
        }

        public TEntity GetOne(Expression<Func<TEntity, bool>> where)
        {
            return Entities.Query().Where(where).FirstOrDefault();
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return Entities.Query();
        }

        public virtual IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where)
        {
            return Entities.Query().Where(where).ToList();
        }

        public int Count(Expression<Func<TEntity, bool>> where)
        {
            return Entities.Query().Where(where).Count();
        }

        public int Count()
        {
            return Entities.Query().Count();
        }

        #endregion
    }
}
