﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ext.Data.Stubs
{
    public class Singleton<T>
    {
        public static T Instance { get; set; }
    }
}
