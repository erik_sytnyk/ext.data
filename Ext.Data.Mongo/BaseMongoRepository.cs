﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Ext.Data.Base;
using Ext.Data.Base.Infrastructure;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Ext.Data.Mongo
{
    public abstract class BaseMongoRepository<TEntity, TContext> : IRepositoryBase<TEntity>
        where TEntity : class
        where TContext : MongoContext, new()
    {
        private readonly MongoCollection _collection;
        private TContext _dataContext;

        protected BaseMongoRepository(TContext context)
        {
            _dataContext = context;
            _collection = DataContext.GetMongoCollection<TEntity>();
        }

        public TContext DataContext
        {
            get { return _dataContext ?? (_dataContext = new TContext()); }
        }

        public void Add(TEntity entity)
        {
            _collection.Insert(entity);
        }

        public void Update(TEntity entity)
        {
            _collection.Save(entity);
        }

        public void Delete(TEntity entity)
        {
            _collection.Remove(Query.EQ("_id", TypeHelper.GetEntityId(entity)));
        }

        public void Delete(int id)
        {
            _collection.Remove(Query.EQ("_id", id));
        }

        public void Delete(Expression<Func<TEntity, bool>> where)
        {
            _collection.Remove(Query<TEntity>.Where(where));
        }

        public TEntity GetById(int id)
        {
            return _collection.FindOneByIdAs<TEntity>(id);
        }

        public TEntity GetOne(Expression<Func<TEntity, bool>> where)
        {
            return _collection.FindOneAs<TEntity>(Query<TEntity>.Where(where));
        }

        public IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where)
        {
            return _collection.FindAs<TEntity>(Query<TEntity>.Where(where));
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _collection.FindAllAs<TEntity>();
        }

        public int Count(Expression<Func<TEntity, bool>> where)
        {
            return (int)_collection.FindAs<TEntity>(Query<TEntity>.Where(where)).Count();
        }

        public int Count()
        {
            return (int)_collection.FindAllAs<TEntity>().Count();
        }
    }
}
