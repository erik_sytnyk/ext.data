﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Ext.Data.Mongo.Infrastructure;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Ext.Data.Mongo
{
    public class MongoContext
    {
        public MongoDatabase Database;
        protected static bool ClassMapsWereRegistered = false;

        public MongoContext(string connectionString, string databaseName)
        {
            var client = new MongoClient(connectionString);
            MongoServer server = client.GetServer();
            Database = server.GetDatabase(databaseName);
        }

        public void BaseClassMapInitialization(BsonClassMap classMap)
        {
            classMap.AutoMap();
            classMap.IdMemberMap.SetIdGenerator(new Int32IdGenerator());
        }

        #region Helper classes

        public List<Type> GetEntityTypes()
        {
            var contextType = this.GetType();

            var result = contextType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(pr => IsMongoCollection(pr.PropertyType))
                .Select(x => GetCollectionType(x.PropertyType))
                .ToList();

            return result;
        }

        public static bool IsMongoCollection(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof (MongoCollection<>);
        }

        public MongoCollection<TEntity> GetMongoCollection<TEntity>()
        {
            var contextType = this.GetType();

            var properties = contextType.GetProperties(BindingFlags.Public | BindingFlags.Instance).ToList();
            var collectionProperties = contextType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(pr => IsMongoCollection(pr.PropertyType));

            foreach (var collectionProperty in collectionProperties)
            {
                var collectionType = GetCollectionType(collectionProperty.PropertyType);

                if (collectionType == typeof (TEntity))
                {
                    return collectionProperty.GetValue(this, null) as MongoCollection<TEntity>;
                }
            }                                

            return null;
        }

        public static Type GetCollectionType(Type type)
        {
            var result = (Type)null;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(MongoCollection<>))
            {
                result = type.GetGenericArguments()[0];
            }

            return result;
        }

        #endregion
    }
}
