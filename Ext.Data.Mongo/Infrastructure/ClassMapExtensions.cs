﻿using System;
using MongoDB.Bson.Serialization;

namespace Ext.Data.Mongo.Infrastructure
{
    public static class ClassMapExtensions
    {
        public static void RegisterClassMap(Type type)
        {
            Type bsonClassMapType = typeof(BsonClassMap<>).MakeGenericType(new Type[] { type });
            BsonClassMap bsonClassMap = (BsonClassMap)Activator.CreateInstance(bsonClassMapType);
            BsonClassMap.RegisterClassMap(bsonClassMap);
        }

        public static void RegisterClassMap(Type type, Action<BsonClassMap> classMapInitializer)
        {
            Type bsonClassMapType = typeof(BsonClassMap<>).MakeGenericType(new Type[] { type });
            BsonClassMap bsonClassMap = (BsonClassMap)Activator.CreateInstance(bsonClassMapType);
            classMapInitializer(bsonClassMap);
            BsonClassMap.RegisterClassMap(bsonClassMap);
        }
    }
}
