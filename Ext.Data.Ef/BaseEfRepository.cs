﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Ext.Data.Base;

namespace Ext.Data.Ef
{
    public abstract class BaseEfRepository<TEntity, TContext> : IRepositoryBase<TEntity> where TEntity : class where TContext : DbContext, new()
    {
        private readonly IDbSet<TEntity> _dbset;
        private TContext _dataContext;

        protected BaseEfRepository(TContext context)
        {
            _dataContext = context;
            _dbset = DataContext.Set<TEntity>();
            CommitAfterEachOperation = true;
        }

        public TContext DataContext
        {
            get { return _dataContext ?? (_dataContext = new TContext()); }
        }

        public bool CommitAfterEachOperation { get; set; }

        #region IRepositoryBase<T> Members

        public virtual void Add(TEntity entity)
        {
            _dbset.Add(entity);
            
            if (CommitAfterEachOperation)
            {
                Commit();
            }            
        }

        public virtual void Update(TEntity entity)
        {
            _dbset.Attach(entity);
            
            _dataContext.Entry(entity).State = EntityState.Modified;
            
            if (CommitAfterEachOperation)
            {
                Commit();
            }   
        }

        public virtual void Delete(TEntity entity)
        {
            _dbset.Remove(entity);

            if (CommitAfterEachOperation)
            {
                Commit();
            }   
        }

        public void Delete(int id)
        {
            var entityToRemove = this.GetById(id);
            _dbset.Remove(entityToRemove);

            if (CommitAfterEachOperation)
            {
                Commit();
            }   
        }

        public void Delete(Expression<Func<TEntity, bool>> where)
        {
            var objects = _dbset.Where(where).AsEnumerable();
            foreach (var obj in objects)
            {
                _dbset.Remove(obj);
            }

            if (CommitAfterEachOperation)
            {
                Commit();
            }
        }

        public virtual TEntity GetById(int id)
        {
            return _dbset.Find(id);
        }

        public TEntity GetOne(Expression<Func<TEntity, bool>> where)
        {
            return _dbset.Where(where).FirstOrDefault();
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _dbset.ToList();
        }

        public virtual IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where)
        {
            return _dbset.Where(where).ToList();
        }

        public int Count(Expression<Func<TEntity, bool>> where)
        {
            return _dbset.Where(where).Count();
        }

        public int Count()
        {
            return _dbset.Count();
        }

        public void Commit()
        {
            _dataContext.SaveChanges();
        }

        #endregion
    }
}
