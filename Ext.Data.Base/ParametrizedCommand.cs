﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace Ext.Data.Base
{
    public class ParametrizedCommand : BaseParametrizedCommand
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ParametrizedCommand()
        { }

        public ParametrizedCommand(string commandTest)
            : this()
        {
            this.CommandText = commandTest;
        }

        private void AddInVarCharParameter(string name, object value)
        {
            this.AddInputParameter(name, DbType.String, value);
        }

        public void AddInIntParameter(string name, object value)
        {
            this.AddInputParameter(name, DbType.Int32, value);
        }

        private void AddInDecimalParameter(string name, object value)
        {
            this.AddInputParameter(name, DbType.Decimal, value);
        }

        /// <summary>
        /// Add int parameter, if value is less then zero, it adds DBNull value
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddInIntNullableParameter(string name, int? value)
        {
            if (value == null)
            {
                this.AddInNullParameter(name);
            }
            else
            {
                this.AddInIntParameter(name, value.Value);
            }
        }

        public void AddInDecimalNullableParameter(string name, decimal? value)
        {
            if (value == null)
            {
                this.AddInNullParameter(name);
            }
            else
            {
                this.AddInDecimalParameter(name, value.Value);
            }
        }

        public void AddInStringParameter(string name, string value)
        {
            this.AddInStringParameter(name, value, true);
        }

        public void AddInStringParameter(string name, string value, bool emptyStringAsNull)
        {
            if (value == null)
            {
                this.AddInNullParameter(name);
            }
            else if (value == String.Empty && emptyStringAsNull)
            {
                this.AddInNullParameter(name);
            }
            else
            {
                this.AddInVarCharParameter(name, value);
            }
        }

        public void AddInDateTimeParameter(string name, object value)
        {
            var dateTime = Convert.ToDateTime(value);
            var defaultDateTime = new DateTime();
            base.AddInputParameter(name, DbType.DateTime,
                                   dateTime == defaultDateTime ? this.GetDefaultPostgresDbDateTime() : dateTime);
        }

        public void AddInNullableDateTimeParameter(string name, DateTime? value)
        {
            if (value == null)
            {
                this.AddInNullParameter(name);
            }
            else
            {
                base.AddInputParameter(name, DbType.DateTime, value.Value);
            }
        }

        private void AddInNullParameter(string name)
        {
            var parameter = new SqlParameter(name, DBNull.Value) {Direction = ParameterDirection.Input};
            this.Parameters.Add(parameter);
        }

        public void AddInBooleanParameter(string paramName, bool paramValue)
        {
            this.AddInputParameter(paramName, DbType.Boolean, paramValue);
        }

        protected override void AddParameter(string name, ParameterDirection direction, object type, object value)
        {
            var parameter = new SqlParameter
                                {ParameterName = name, DbType = (DbType) type, Direction = direction, Value = value};
            this.Parameters.Add(parameter);
        }

        public void AddParameter(string name, DbType type, object value)
        {
            var parameter = new SqlParameter
                                {
                                    ParameterName = name,
                                    DbType = type,
                                    Direction = ParameterDirection.Input,
                                    Value = value
                                };
            this.Parameters.Add(parameter);
        }

        //private String ConvertDateTimeToStringInPostgresFormat(DateTime dateTime)
        //{
        //    //'2004-10-19 10:23:54'
        //    return String.Format("'{0}-{1}-{2} {3}:{4}:{5}'", dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
        //}

        private DateTime GetDefaultPostgresDbDateTime()
        {
            return new DateTime(1984, 1, 1);
        }

        #region get actual sql query string

        private string GetActualQueryText()
        {
            var result = this.CommandText;
            return this.Parameters.Aggregate(result, (current, param) => current.Replace(param.ParameterName, GetInsertParameterValue(param)));
        }

        //for debug to see which tables are requested
        public string GetFromCommandPart()
        {
            var result = this.GetActualQueryText();
            var indexOfFrom = result.IndexOf("FROM", StringComparison.InvariantCultureIgnoreCase);
            result = result.Substring(indexOfFrom);
            return result;
        }

        private string GetInsertParameterValue(DbParameter param)
        {
            switch (param.DbType)
            {
                case DbType.String:
                case DbType.AnsiString:
                    return String.IsNullOrEmpty(param.Value.ToString()) ? "NULL" : String.Format("'{0}'", param.Value);
                case DbType.Int32:
                    return String.Format("{0}", String.IsNullOrEmpty(param.Value.ToString()) ? "NULL" : param.Value.ToString());
                case DbType.DateTime:
                case DbType.Date:
                    return (DateTime)param.Value == DateTime.MinValue ? "NULL" : this.GetTimeStampValueFromDateTime((DateTime)param.Value);
                case DbType.Boolean:
                    return (bool)param.Value ? "true" : "false";
            }
            return "not supported format";
        }

        private string GetTimeStampValueFromDateTime(DateTime dateTime)
        {
            return String.Format("'{0}-{1}-{2} {3}:{4}:{5}'", dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour,
                                 dateTime.Minute, dateTime.Second);
        }

        #endregion

        public void AddInFloatParameter(string name, float value)
        {
            this.AddInputParameter(name, DbType.Decimal, value);
        }
    }
}
