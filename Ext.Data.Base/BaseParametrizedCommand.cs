#region Using

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

#endregion

namespace Ext.Data.Base
{
    public abstract class BaseParametrizedCommand
    {
        #region Fields

        private readonly List<DbParameter> _parameters;

        #endregion

        #region Properties

        public List<DbParameter> Parameters
        {
            get { return this._parameters; }
        }

        public string CommandText { get; set; }

        protected abstract void AddParameter(string name, ParameterDirection direction, object type, object value);

        #endregion

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected BaseParametrizedCommand()
        {
            this.CommandText = String.Empty;
            this._parameters = new List<DbParameter>();
        }

        protected void AddInputParameter(string name, object type, object value)
        {
            this.AddParameter(name, ParameterDirection.Input, type, value);
        }

        protected internal void AddOutputParameter(string name, DbType type, object value)
        {
            this.AddParameter(name, ParameterDirection.Output, type, value);
        }

        #region get actual sql query string

        public string GetActualQueryText()
        {
            string result = this.CommandText;
            foreach (var param in this.Parameters)
            {
                result = result.Replace(param.ParameterName, GetInsertParameterValue(param));
            }
            return result;
        }

        //for debug to see which tables are requested
        public string GetFromCommandPart()
        {
            string result = this.GetActualQueryText();
            int indexOfFrom = result.IndexOf("FROM", StringComparison.InvariantCultureIgnoreCase);
            result = result.Substring(indexOfFrom);
            return result;
        }

        private string GetInsertParameterValue(DbParameter param)
        {
            switch (param.DbType)
            {
                case DbType.String:
                case DbType.AnsiString:
                    return String.IsNullOrEmpty(param.Value.ToString()) ? "NULL" : String.Format("'{0}'", param.Value);
                case DbType.Int32:
                    return String.Format("{0}", String.IsNullOrEmpty(param.Value.ToString()) ? "NULL" : param.Value.ToString());
                case DbType.DateTime:
                case DbType.Date:
                    return (DateTime)param.Value == DateTime.MinValue ? "NULL" : this.GetTimeStampValueFromDateTime((DateTime)param.Value);
                case DbType.Boolean:
                    return (bool)param.Value ? "true" : "false";
            }
            return "not supported format";
        }

        private string GetTimeStampValueFromDateTime(DateTime dateTime)
        {
            return String.Format("'{0}-{1}-{2} {3}:{4}:{5}'", dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour,
                                 dateTime.Minute, dateTime.Second);
        }

        #endregion
    }
}