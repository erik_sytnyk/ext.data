﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ext.Core;

namespace Ext.Data.Base.Infrastructure
{
    //TODO move to Ext.Core as ReflectionHelper or something like that
    public static class TypeHelper
    {
        private static readonly IDictionary<Type, IEnumerable<PropertyInfo>> PropertiesCache = new Dictionary<Type, IEnumerable<PropertyInfo>>();

        private static readonly object Lock = new object();

        public static IEnumerable<PropertyInfo> GetProperties(Type type)
        {
            if (!PropertiesCache.ContainsKey(type))
            {
                lock (Lock)
                {
                    if (!PropertiesCache.ContainsKey(type))
                    {
                        var props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                        PropertiesCache.Add(type, props);
                    }
                }
            }
            return PropertiesCache[type];
        }

        public static PropertyInfo GetEntityIdProperty(Type type)
        {
            var typeNamePlusId = String.Format("{0}Id", type.Name);

            var result = TypeHelper.GetProperties(type).SingleOrDefault(x => x.Name.Equals("id", StringComparison.OrdinalIgnoreCase) || x.Name.Equals(typeNamePlusId, StringComparison.OrdinalIgnoreCase));

            Check.That(result != null, String.Format("Entity type '{0}' does not have id property", type.Name));

            Check.That(result.PropertyType == typeof(int), "Entity id is not of type integer");

            return result;
        }

        public static int GetEntityId(object entity)
        {
            var idProperty = GetEntityIdProperty(entity.GetType());

            return (int)idProperty.GetValue(entity, null);
        }
    }
}