using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Ext.Data.Base
{
    /// <summary>
    /// Only used for old DB need set of modifications to be used in new context
    /// </summary>
    public abstract class BaseDao
    {
        private static SqlConnection _globalConnection;
        private static SqlTransaction _globalTransaction;
        private static bool _useGlobalTransaction;
        private int _commandTimeout = -1;
        private string _connectionString = String.Empty;
        private int _lastSuccessfulCommandIndex = -1;

        #region Properties

        private static SqlConnection GlobalConnection
        {
            get { return _globalConnection; }
            set { _globalConnection = value; }
        }

        private static SqlTransaction GlobalTransaction
        {
            get { return _globalTransaction; }
            set { _globalTransaction = value; }
        }

        private static bool UseGlobalTransaction
        {
            get { return _useGlobalTransaction; }
            set { _useGlobalTransaction = value; }
        }

        private string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        private int CommandTimeout
        {
            get { return _commandTimeout; }
            set { _commandTimeout = value; }
        }

        public bool GlobalTransactionIsInitialized
        {
            get { return GlobalConnection != null && GlobalTransaction != null; }
        }

        #endregion

        protected BaseDao(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        protected String ConvertBoolToYN(bool flag)
        {
            return flag ? "Y" : "N";
        }

        protected static bool ConvertYNStrToBool(string str)
        {
            return str == "Y";
        }

        protected string GetString(DataRow row, string columnName)
        {
            var isNull = row[columnName] is DBNull || (string)row[columnName] == String.Empty || (string)row[columnName] == " ";
            var result = isNull ? null : Convert.ToString(row[columnName]);
            return result;
        }

        #region Data Access Region

        protected DataTable GetDataTable(BaseParametrizedCommand command)
        {
            var connection = new SqlConnection(this.ConnectionString);
            var result = new DataTable();
            try
            {
                var dataAdapter = new SqlDataAdapter();
                connection.Open();
                var dbCommand = connection.CreateCommand();
                if (this.CommandTimeout != -1)
                {
                    dbCommand.CommandTimeout = CommandTimeout;
                }
                dbCommand.CommandText = command.CommandText;
                foreach (SqlParameter parameter in command.Parameters)
                {
                    dbCommand.Parameters.Add(parameter);
                }
                dataAdapter.SelectCommand = dbCommand;
                dataAdapter.Fill(result);
            }
            catch (Exception e)
            {
                LogSQLError(e, command);
                throw;
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        protected object ExecuteScalar(BaseParametrizedCommand command)
        {
            var commandSql = new SqlCommand();
            if (this.CommandTimeout != -1)
            {
                commandSql.CommandTimeout = CommandTimeout;
            }
            commandSql.CommandText = command.CommandText;
            SqlConnection connection = null;
            SqlTransaction transaction = null;
            try
            {
                //if global transaction is not used, we do not use transaction at all
                if (!UseGlobalTransaction)
                {
                    connection = new SqlConnection(this.ConnectionString);
                }
                else
                {
                    connection = GlobalConnection;
                    transaction = GlobalTransaction;
                    commandSql.Transaction = transaction;
                }

                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                commandSql.Connection = connection;

                foreach (var param in command.Parameters)
                {
                     commandSql.Parameters.AddWithValue(param.ParameterName, param.Value);
                }

                return commandSql.ExecuteScalar();
            }
            catch (Exception ex)
            {
                if (!UseGlobalTransaction)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                }
                var  e = new Exception(LogSQLError(ex, command));
                throw e;
            }
            finally
            {
                if (!UseGlobalTransaction)
                {
                    if (connection != null && connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    if (connection != null) connection.Dispose();
                }
            }
        }

        protected void ExecuteNonQueryWithoutTransaction(BaseParametrizedCommand command)
        {
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(this.ConnectionString);
                connection.Open();
                var commandSql = connection.CreateCommand();
                if (this.CommandTimeout != -1)
                {
                    commandSql.CommandTimeout = CommandTimeout;
                }
                commandSql.CommandText = command.CommandText;
                foreach (var param in command.Parameters)
                {
                    commandSql.Parameters.AddWithValue(param.ParameterName, param.Value);
                }
                commandSql.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var e = new Exception(LogSQLError(ex, command));
                throw e;
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                if (connection != null) connection.Dispose();
            }
        }

        protected void ExecuteNonQuery(BaseParametrizedCommand command)
        {
            ExecuteNonQuery(new[] { command });
        }

        private void ExecuteNonQuery(params BaseParametrizedCommand[] commands)
        {
            SqlConnection connection = null;
            SqlTransaction transaction = null;
            try
            {
                if (!UseGlobalTransaction)
                {
                    connection = new SqlConnection(this.ConnectionString);
                    connection.Open();
                    transaction = connection.BeginTransaction();
                }
                else
                {
                    connection = GlobalConnection;
                    transaction = GlobalTransaction;
                }
                _lastSuccessfulCommandIndex = -1;
                foreach (var command in commands)
                {
                    var commandSql = connection.CreateCommand();
                    if (this.CommandTimeout != -1)
                    {
                        commandSql.CommandTimeout = CommandTimeout;
                    }
                    commandSql.Transaction = transaction;
                    commandSql.CommandText = command.CommandText;
                    foreach (var param in command.Parameters)
                    {
                        commandSql.Parameters.AddWithValue(param.ParameterName, param.Value);
                    }
                    commandSql.ExecuteNonQuery();
                    _lastSuccessfulCommandIndex++;
                }
                if (!UseGlobalTransaction)
                {
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                //roll back if global transaction is not used, if it is used it should be done somewhere outside
                if (!UseGlobalTransaction)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                }
                var e = new Exception(LogSQLError(ex, commands[_lastSuccessfulCommandIndex + 1]));
                throw e;
            }
            finally
            {
                if (!UseGlobalTransaction)
                {
                    if (connection != null && connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    if (connection != null) connection.Dispose();
                }
            }
        }

        public void StartGlobalTransaction(SqlConnection connection, SqlTransaction transaction)
        {
            UseGlobalTransaction = true;
            GlobalConnection = connection;
            GlobalTransaction = transaction;
        }

        public void CommitGlobalTransaction()
        {
            _useGlobalTransaction = false;
            if (_globalTransaction != null)
            {
                _globalTransaction.Commit();
            }
            if (_globalConnection != null)
            {
                _globalConnection.Close();
            }
            _globalConnection = null;
            _globalTransaction = null;
        }

        public void RollbackGlobalTransaction()
        {
            _useGlobalTransaction = false;
            if (_globalTransaction != null)
            {
                _globalTransaction.Rollback();
            }
            if (_globalConnection != null)
            {
                _globalConnection.Close();
            }
            _globalConnection = null;
            _globalTransaction = null;
        }

        public SqlTransaction GetGlobalTransaction()
        {
            return GlobalTransaction;
        }

        #endregion

        #region Utility functions

        public static string GetSqlFilterIdInRange(string columnName, int[] ids)
        {
            string result = String.Empty;
            if (ids.Length != 0)
            {
                StringBuilder idsStr = new StringBuilder();
                foreach (int id in ids)
                {
                    idsStr.Append(String.Format("{0}, ", id));
                }
                idsStr.Remove(idsStr.Length - 2, 1);
                result = String.Format("{0} in ({1})", columnName, idsStr);
            }
            return result;
        }

        public static int[] GetLocalizedIdsByColumnNames(DataRow row, string[] columnNames)
        {
            List<int> ids = new List<int>();
            foreach (string name in columnNames)
            {
                int localizedId = row.IsNull(name) ? -1 : Convert.ToInt32(row[name]);
                if (localizedId > 0 && !ids.Contains(localizedId))
                {
                    ids.Add(localizedId);
                }
            }

            return ids.ToArray();
        }

        #endregion

        #region Private

        private void LogException(Exception ex, string extendedMessage)
        {
            throw new Exception(String.Format(ex.Message, extendedMessage));
        }

        private void LogException(Exception ex)
        {
            LogException(ex, ex.Message);
        }

        private static string LogSQLError(Exception ex, BaseParametrizedCommand command)
        {
            var strOut = ex.Message + Environment.NewLine + "SQL query:" + Environment.NewLine + command.CommandText;

            strOut = strOut + Environment.NewLine + Environment.NewLine +
                     "Input parameters:" + Environment.NewLine;

            strOut = command.Parameters.Aggregate(strOut, (current, param) => current + param.ParameterName + ": " + param.Value + Environment.NewLine);

            strOut += Environment.NewLine;
            strOut += "Actual query: " + command.CommandText;

            //LogException(ex, strOut);
            return strOut;
        }

        #endregion
    }
}
