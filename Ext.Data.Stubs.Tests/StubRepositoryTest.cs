﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ext.Data.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StubDb;

namespace Ext.Data.Stubs.Tests
{
    [TestClass]
    public class StubRepositoryTest
    {
        #region Nested classes

        public class Project
        {
            public int Id { get; set; }
            public string Name { get; set; }

        }

        public class Person
        {
            public int Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }

        public class StubTestContext : StubContext
        {
            public StubSet<Project> Projects { get; set; }
            public StubSet<Person> Persons { get; set; }
        }

        public class RepositoryStubBase<TEntity> : BaseStubRepository<TEntity, StubTestContext> where TEntity : class 
        {
            public RepositoryStubBase() : base(new StubTestContext())
            {
            }
        }

        public class ProjectRepository: RepositoryStubBase<Project>
        {
            public int GetPersonsCount()
            {
                return DataContext.Persons.Query().Count();
            }
        }

        public class PersonsRepository : RepositoryStubBase<Person>
        {

        }




        #endregion

        [TestMethod]
        public void repositories_should_operate_on_the_same_context()
        {
            var personRepository = new PersonsRepository();
            personRepository.Add(new Person(){FirstName = "Yegor", LastName = "Sytnyk"});

            var anotherInstance = new PersonsRepository();

            var personsCount = new ProjectRepository().GetPersonsCount();
            //var personsCount = anotherInstance.GetAll().Count();

            Assert.AreEqual(1, personsCount);
            
        }
    }
}
